package com.cinema4you.home.presentation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.cinema4.libraries.networking.ImageUrlBuilder
import com.cinema4you.home.gateway.HomeViewModel
import com.cinema4you.libraries.core.domain.model.Movie
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState

@Composable
fun HomeView(viewModel: HomeViewModel) {
    val uiViewState =  viewModel.headHomeState
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AnimatedVisibility(
            visible = uiViewState.loading,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            LinearProgressIndicator()
        }
        AnimatedVisibility(visible = uiViewState.loading.not()) {
            HomeHead(uiViewState.data)
        }
    }
}


@OptIn(ExperimentalPagerApi::class)
@Composable
private fun HomeHead(movies: List<Movie>) {
    val pagerState = rememberPagerState()

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        HorizontalPager(
            count = movies.size,
            state = pagerState
        ) { page ->
            HomeHeadImageView(movies[page])
        }

        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .padding(4.dp)
                .wrapContentWidth()
                .wrapContentHeight()
        )
    }
}

@Composable
private fun HomeHeadImageView(movie: Movie) {
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(contentAlignment = Alignment.BottomCenter) {

            AsyncImage (
                model = ImageRequest.Builder(LocalContext.current)
                    .data(movie?.backdropPath?.let { ImageUrlBuilder.buildBackdropUrl(it)})
                    .crossfade(true)
                    .build(),
                contentDescription = "test",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .height(200.dp)
                    .fillMaxWidth()
                    // .clip(RoundedCornerShape(10.dp))
            )

            Text(
                text = movie.title,
                Modifier
                    .fillMaxWidth()
                    .height(60.dp)
                    .padding(8.dp)
                    .background(Color.LightGray.copy(alpha = 0.60F))
                    .padding(8.dp),
                textAlign = TextAlign.Start,
                fontSize = 18.sp,
                fontWeight = FontWeight.Medium
            )
        }
    }
}

@Composable
private fun ButtonActions(
    data: List<String>
) {
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    // HomeHead()
}