package com.cinema4you.features.movie.list.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.cinema4you.features.movie.R

class MovieDetailFragment: Fragment() {

    val args: MovieDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.content_detail_movie_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val movie = args.movie
        // movieTitle.text = movie.title
    }
}