package com.cinema4you.home.domain.interactor

import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.libraries.core.domain.executor.CoroutineUseCase
import com.cinema4you.libraries.core.domain.model.Movie

class GetPopularMoviesUseCase constructor(private val repository: HomeRepository
) : CoroutineUseCase<CoroutineUseCase.None, List<Movie>>() {
    override suspend fun execute(param: None?): List<Movie> {
        return repository.getPopularMovies()
    }
}