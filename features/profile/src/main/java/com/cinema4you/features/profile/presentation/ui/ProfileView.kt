package com.cinema4you.features.profile.presentation.ui

import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.cinema4you.features.profile.R
import com.cinema4you.features.profile.gateway.ProfileViewModel
import com.cinema4you.features.profile.domain.model.Profile
import com.cinema4you.libraries.designsystem.presentation.ui.bigPadding
import com.cinema4you.libraries.designsystem.presentation.ui.gigaPadding
import com.cinema4you.libraries.designsystem.presentation.ui.mediumPadding
import com.cinema4you.libraries.designsystem.presentation.ui.smallPadding

@Composable
fun ProfileView(viewModel: ProfileViewModel) {

    val profileUiState = viewModel.profileState

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
        ) {

        ProfileHead()
        ProfileBody(profileUiState.data)
    }
}

@Composable
private fun ProfileHead() {
    Text(
        text = stringResource(R.string.app_name),
        color = Color.Blue,
    )
    Spacer(modifier = Modifier.height(bigPadding))
    Image(
        painter = painterResource(id = R.drawable.ic_launcher_foreground),
        contentDescription = "teste",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(64.dp)
            .clip(CircleShape),
    )
    Spacer(modifier = Modifier.height(gigaPadding))
}

@Composable
private fun ProfileBody(profile: Profile) {
    Card (
        modifier = Modifier
            .wrapContentWidth()
            .padding(bigPadding),
        elevation = mediumPadding
    ) {
        Column {
            ProfileItemText(
                R.string.profile_title_name,
                profile.name
            )
            ProfileItemText(
                R.string.profile_title_email,
                profile.email
            )
            ProfileItemText(
                R.string.profile_title_country,
                profile.country
            )
        }
    }
}

@Composable
private fun ProfileItemText(
    @StringRes title: Int,
    value: String
) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(gigaPadding, 0.dp, gigaPadding, 0.dp)
    ) {
        Text(
            text = stringResource(title),
            color = Color.Blue,
            modifier = Modifier
                .padding(0.dp, smallPadding, gigaPadding, smallPadding)
        )
        Text(
            text = value,
            color = Color.Cyan,
            textAlign = TextAlign.End,
            modifier = Modifier
                .padding(mediumPadding, smallPadding, 0.dp, smallPadding)
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    // val viewModel by viewModel<ProfileViewModel>()
    // ProfileView()
}