package com.cinema4you.features.profile.gateway

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cinema4you.features.profile.domain.interactor.GetUserProfileUseCase
import com.cinema4you.features.profile.domain.model.Profile

class ProfileViewModel constructor(val getUserProfileUseCase: GetUserProfileUseCase): ViewModel() {

    var profileState by mutableStateOf(UiState())

    fun getProfileData() {
        getUserProfileUseCase(viewModelScope) {
            success = { profileState = UiState(it) }
        }
    }
}