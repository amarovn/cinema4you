package com.cinema4you.home.gateway

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cinema4you.home.domain.interactor.GetLastMovieUseCase
import com.cinema4you.home.domain.interactor.GetUpcomingMovieUseCase
import com.cinema4you.libraries.core.domain.model.Movie

class HomeViewModel constructor(
    private val getLatestMovieUseCase: GetLastMovieUseCase,
    private val getUpcomingMovieUseCase: GetUpcomingMovieUseCase
): ViewModel() {

    var headHomeState by mutableStateOf(HeadHomeUiState())

    fun loadLastMovie() {
        getLatestMovieUseCase(viewModelScope) {
            success = { handleLastMovieUseCase(it) }
        }
    }

    fun loadUpcomingMovies() {
        getUpcomingMovieUseCase {
            success = { handleUpcomingMovie(it) }
        }
    }

    private fun handleUpcomingMovie(movies: List<Movie>) {
        headHomeState = HeadHomeUiState(movies, loading = false)
    }

    private fun handleLastMovieUseCase(movie: Movie?) {
        // headHomeState = HeadHomeUiState(movies)
    }
}