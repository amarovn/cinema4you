package com.cinema4you.home.gateway

import com.cinema4you.libraries.core.domain.model.Movie

data class HeadHomeUiState(
    val data: List<Movie> = emptyList(),
    val loading: Boolean = true
)