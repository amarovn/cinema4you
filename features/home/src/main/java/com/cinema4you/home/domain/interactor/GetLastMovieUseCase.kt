package com.cinema4you.home.domain.interactor

import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.libraries.core.domain.executor.CoroutineUseCase
import com.cinema4you.libraries.core.domain.model.Movie

class GetLastMovieUseCase constructor(
    private val repository: HomeRepository
) : CoroutineUseCase<CoroutineUseCase.None, Movie?>() {
    override suspend fun execute(param: None?): Movie? {
        return repository.getLastMovie()
    }
}