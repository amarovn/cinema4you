package com.cinema4you.features.profile.plugin.di

import com.cinema4you.features.profile.domain.repository.ProfileRepository
import com.cinema4you.features.profile.plugin.repository.ProfileRepositoryImpl
import org.koin.dsl.module

val profileRepositoryModule = module {
    single<ProfileRepository> { ProfileRepositoryImpl() }
}