package com.cinema4you.features.movie.plugin

import android.content.Context
import com.cinema4.libraries.networking.api.MovieServiceAPI
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.Mockito

class RepositoryTestRule : TestRule {

    val movieServiceAPI: MovieServiceAPI = Mockito.mock(MovieServiceAPI::class.java)
    val context: Context = Mockito.mock(Context::class.java)

    override fun apply(base: Statement?, description: Description?): Statement {
        return object : Statement() {
            override fun evaluate() {
                base?.evaluate()
            }
        }
    }
}