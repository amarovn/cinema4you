package com.cinema4you.libraries.designsystem.presentation.ui

import androidx.compose.ui.unit.dp

val smallPadding = 4.dp
val mediumPadding = 8.dp
val bigPadding = 12.dp
val gigaPadding = 16.dp