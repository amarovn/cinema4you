package com.cinema4you.features.movie.plugin.di

import com.cinema4you.features.movie.plugin.repository.AppDataBase
import org.koin.dsl.module

val daoModule = module {
    single { AppDataBase.getInstanceDatabase(get()) }

    single { get<AppDataBase>().movieDao() }
}