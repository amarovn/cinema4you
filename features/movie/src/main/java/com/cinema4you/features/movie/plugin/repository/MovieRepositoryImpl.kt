package com.cinema4you.features.movie.plugin.repository

import android.content.Context
import com.cinema4.libraries.networking.NetworkUtils
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieLocalRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRemoteRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe

class MovieRepositoryImpl(
    private val context: Context,
    private val networkUtils: NetworkUtils,
    private val remoteRepository: MovieRemoteRepository,
    private val localRepository: MovieLocalRepository
): MovieRepository {
    @Deprecated("use coroutine flow")
    override fun getPopularMovies(): Flowable<List<Movie>> {
        return if (cached().not() && internetConnection()) {
            remoteRepository.fetchMovies().toFlowable()
                .flatMap { movies -> updateMoviesDB(movies) }
                .flatMap { movies -> Flowable.fromCallable { movies } }
        } else {
            localRepository.getAllMovies()
        }
    }

    override fun getMovie(id: Int): Maybe<Movie> {
        TODO("Not yet implemented")
    }

    override suspend fun getPopularMoviesList(): List<Movie> {
        return if (cached().not() && internetConnection()) {
            val movies = remoteRepository.fetchMoviesFromRemote()
            updateMovies(movies)
            return movies
        } else {
            localRepository.getAll()
        }
    }

    private fun updateMoviesDB(movies: List<Movie>): Flowable<List<Movie>> {
        return localRepository.deleteAllMovies()
            .andThen(localRepository.saveMovies(movies))
            .andThen(Flowable.fromCallable{movies})
    }

    private suspend fun updateMovies(movies: List<Movie>) {
        localRepository.run {
            deleteAll()
            save(movies)
        }
    }

    //TODO created extension?
    private fun cached() = false

    private fun internetConnection() = networkUtils.hasInternetConnection(context)
}