package com.cinema4you.libraries.core.domain.executor

import androidx.annotation.VisibleForTesting
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class CoroutineUseCase<P, R : Any?>{

    operator fun invoke (param: P?,
                         scope: CoroutineScope = GlobalScope,
                         subscribe: CoroutineDispatcher = Dispatchers.IO,
                         observeOn: CoroutineDispatcher = Dispatchers.Main,
                         onResult: UseCaseResult<R>.() -> Unit
    ) {
        runUseCase(
            param,
            scope,
            subscribe,
            observeOn,
            onResult
        )
    }

    operator fun invoke(scope: CoroutineScope = GlobalScope,
                        subscribe: CoroutineDispatcher = Dispatchers.IO,
                        observeOn: CoroutineDispatcher = Dispatchers.Main,
                        onResult: UseCaseResult<R>.() -> Unit
    ) {
        runUseCase(
            null,
            scope,
            subscribe,
            observeOn,
            onResult
        )
    }

    private fun runUseCase(
        param: P?,
        scope: CoroutineScope = GlobalScope,
        subscribe: CoroutineDispatcher = Dispatchers.IO,
        observeOn: CoroutineDispatcher = Dispatchers.Main,
        onResult: UseCaseResult<R>.() -> Unit
    ) {
        scope.launch(observeOn) {
            val result = UseCaseResult<R>()
            result.onResult()
            withContext(subscribe) {
                runCatching {
                    execute(param)
                }.onSuccess {
                    result.onSuccess(it)
                }.onFailure {
                    result.onFail(it)
                }
            }
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    suspend fun process(param: P? = null): R {
        return execute(param)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    abstract suspend fun execute(param: P?): R

    class UseCaseResult<R> {
        var success: ((R) -> Unit)? = null
        var fail: ((Throwable) -> Unit)? = null

        fun onSuccess(param: R) {
            success?.invoke(param)
        }
        fun onFail(param: Throwable) {
            fail?.invoke(param)
        }
    }

    object None
}

