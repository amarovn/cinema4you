package com.cinema4you.features.profile.gateway

import com.cinema4you.features.profile.domain.interactor.GetUserProfileUseCase
import com.cinema4you.features.profile.domain.model.Profile
import com.cinema4you.features.profile.plugin.MainDispatcherRule
import com.cinema4you.features.profile.plugin.mockWithSuccess
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ProfileViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val useCase = mockk<GetUserProfileUseCase>()
    private lateinit var target : ProfileViewModel

    @Before
    fun setUp() {
        target = ProfileViewModel(useCase)
    }

    @Test
    fun `when getProfileData then should return profile object`() = runTest {
        val profile = Profile("Amaro")

        useCase.mockWithSuccess(profile)

        target.getProfileData()
        assertEquals(target.profileState, UiState(profile))
    }
}