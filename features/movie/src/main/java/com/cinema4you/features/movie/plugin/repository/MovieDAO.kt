package com.cinema4you.features.movie.plugin.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cinema4you.features.movie.base.domain.model.MovieDB
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

@Dao
interface MovieDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movies :List<MovieDB>): Completable

    @Query("DELETE FROM movie")
    fun deleteAllMovies(): Completable

    @Query("SELECT * from movie")
    fun getAllMovies(): Flowable<List<MovieDB>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(movies: List<MovieDB>)

    @Query("SELECT * from movie")
    suspend fun getAll(): List<MovieDB>

    @Query("DELETE FROM movie")
    suspend fun deleteAll()
}