import org.gradle.api.JavaVersion

object Config {
    const val minSdk = 24
    const val compileSdk = 31
    const val targetSdk = 31
    val javaVersion = JavaVersion.VERSION_11
    const val buildTools = "30.0.3"
    const val jvmTarget = "11"
    const val kotlinCompilerExtensionVersion = "1.1.1"
}

object Versions {
    //A
    const val androidx_core = "1.7.0"
    const val androidx_compact = "1.4.1"
    const val androidx_material = "1.6.1"
    const val androidx_junit = "1.1.2"
    const val androidx_espresso = "3.4.0"
    const val androidx_core_testing = "2.1.0"
    const val androidx_fragment_testing = "1.4.1"
    const val android_gradle = "7.2.1"

    //C
    const val coroutine = "1.4.1"
    const val coroutines_version = "1.6.3"
    const val compose = "1.1.1"
    const val compose_activity = "1.4.0"
    const val compose_viewmodel = "2.4.1"
    const val coil = "2.1.0"

    //G
    const val gson = "2.9.0"
    const val glide = "4.13.0"

    //K
    const val kotlin_gradleplugin = "1.6.21"
    const val koin = "3.2.0"
    const val koin_workermanager = "3.2.0"

    //M
    const val mockito = "4.0.0"
    const val mockito_inline = "4.6.0"
    const val material_design = "1.6.1"

    //N
    const val navigation = "2.4.2"
    const val navigation_plugin = "2.4.2"

    //O
    const val okhttp = "4.9.3"

    //R
    const val rxandroid = "3.0.0"
    const val rxjava3 = "3.1.5"
    const val retrofit = "2.9.0"
    const val retrofit_converse_gson = "2.9.0"
    const val retrofit_rxjava3 = "2.9.0"
    const val room = "2.4.2"
    const val room_rxjava3 = "2.4.1"
    const val room_compiler = "2.4.1"
    const val room_ktx = "2.4.2"
}

object Deps {
    //ANDROIDX
    const val androidx_core = "androidx.core:core-ktx:${Versions.androidx_core}"
    const val androidx_compact = "androidx.appcompat:appcompat:${Versions.androidx_compact}"
    const val androidx_material = "com.google.android.material:material:${Versions.androidx_material}"
    const val androidx_junit = "androidx.test.ext:junit:${Versions.androidx_junit}"
    const val androidx_espresso = "androidx.test.espresso:espresso-core:${Versions.androidx_espresso}"
    const val androidx_core_testing =  "androidx.arch.core:core-testing:${Versions.androidx_core_testing}"
    const val androidx_fragment_testing = "androidx.fragment:fragment-testing:${Versions.androidx_fragment_testing}"

    //COIL
    const val coil = "io.coil-kt:coil-compose:${Versions.coil}"

    //COMPOSE
    const val compose_ui = "androidx.compose.ui:ui:${Versions.compose}"
    const val compose_ui_tooling = "androidx.compose.ui:ui-tooling:${Versions.compose}"
    const val compose_foundation = "androidx.compose.foundation:foundation:${Versions.compose}"
    const val compose_material = "androidx.compose.material:material:${Versions.compose}"
    const val compose_material_icons_core = "androidx.compose.material:material-icons-core:${Versions.compose}"
    const val compose_material_icons_extended = "androidx.compose.material:material-icons-extended:${Versions.compose}"
    const val compose_activity = "androidx.activity:activity-compose:${Versions.compose_activity}"
    const val compose_viewmodel = "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.compose_viewmodel}"
    const val compose_livedata = "androidx.compose.runtime:runtime-livedata:${Versions.compose}"

    // COROUTINES
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine}"
    const val coroutines_test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines_version}"

    //TEST
    const val junit = "junit:junit:4.+"

    //MATERIAL DESIGN
    const val material = "com.google.android.material:material:${Versions.material_design}"

    // MOCKITO
    const val mockito = "org.mockito.kotlin:mockito-kotlin:${Versions.mockito}"
    const val mockito_inline = "org.mockito:mockito-inline:${Versions.mockito_inline}"

    //KOIN
    const val koin = "io.insert-koin:koin-android:${Versions.koin}"
    const val koin_workermanager = "io.insert-koin:koin-androidx-workmanager:${Versions.koin_workermanager}"

    //RX
    const val rxandroid = "io.reactivex.rxjava3:rxandroid:${Versions.rxandroid}"
    const val rxjava3 = "io.reactivex.rxjava3:rxjava:${Versions.rxjava3}"

    //RETROFIT
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofit_converse_gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_converse_gson}"
    const val retrofit_rxjava3 = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit_rxjava3}"
    const val okhttp = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"

    //NAVIGATION
    const val navigation_fragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigation_ui = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val navigation_testing = "androidx.navigation:navigation-testing:${Versions.navigation}"
    const val navigation_dynamic_feature = "androidx.navigation:navigation-dynamic-features-fragment:${Versions.navigation}"
    const val navigation_compose = "androidx.navigation:navigation-compose:${Versions.navigation}"

    //ROOM
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val room_rxjava3 = "androidx.room:room-rxjava3:${Versions.room_rxjava3}"
    const val room_compiler = "androidx.room:room-compiler:${Versions.room_compiler}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val room_ktx = "androidx.room:room-ktx:${Versions.room_ktx}"

    //GRADLE
    const val tools_gradleandroid = "com.android.tools.build:gradle:gradle-${Versions.android_gradle}"
    const val tools_kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_gradleplugin}"
    const val tools_gradlenavigation = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation_plugin}"

    //GLIDE
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glide_annotation = "com.github.bumptech.glide:compiler:${Versions.glide}"
}