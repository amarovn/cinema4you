package com.cinema4you.features.movie.plugin.di


import com.cinema4.libraries.networking.NetworkUtils
import com.cinema4.libraries.networking.RetrofitConfig
import com.cinema4.libraries.networking.RetrofitConfigImpl
import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.model.MovieDB
import com.cinema4you.features.movie.base.domain.repository.MovieLocalRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRemoteRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.features.movie.plugin.mapper.MovieDBListMapper
import com.cinema4you.features.movie.plugin.mapper.MovieListToMovieDBListMapper
import com.cinema4you.features.movie.plugin.mapper.MovieRemoteListMapper
import com.cinema4you.features.movie.plugin.mapper.MovieRemoteMapper
import com.cinema4you.features.movie.plugin.mapper.MovieRemoteNullableInputListMapper
import com.cinema4you.features.movie.plugin.mapper.MovieToMovieDBMapper
import com.cinema4you.features.movie.plugin.repository.MovieLocalRepositoryImpl
import com.cinema4you.features.movie.plugin.repository.MovieRemoteRepositoryImpl
import com.cinema4you.features.movie.plugin.repository.MovieRepositoryImpl
import com.cinema4you.libraries.core.domain.mapper.ListMapper
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.mapper.NullableInputListMapper
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {
    single<MovieRepository> { MovieRepositoryImpl(get(),get(), get(), get()) }
    single<MovieRemoteRepository> { MovieRemoteRepositoryImpl(get(), get(), get(named("movieDBtoMovieMapper"))) }
    single<MovieLocalRepository> { MovieLocalRepositoryImpl(get(), get(named("movieDBToMovieMapper")), get(named("movieListToMovieDBMapper"))) }
    single<RetrofitConfig> { RetrofitConfigImpl() }
    single<ListMapper<MovieDTO, Movie>>(named("remoteMapper")) { MovieRemoteListMapper() } // bind MovieRemoteListMapper::class
    single<ListMapper<MovieDB, Movie>>(named("movieDBMapper")) { MovieDBListMapper() }
    single<Mapper<MovieDB, Movie>>(named("movieDBToMovieMapper")) { MovieToMovieDBMapper() }
    single<ListMapper<Movie, MovieDB>>(named("movieListToMovieDBMapper")) { MovieListToMovieDBListMapper() }
    single<NullableInputListMapper<MovieDTO, Movie>> { MovieRemoteNullableInputListMapper() }
    factory <Mapper<MovieDTO, Movie>>(named("movieDBtoMovieMapper")) { MovieRemoteMapper() }

    single{ NetworkUtils() }
}