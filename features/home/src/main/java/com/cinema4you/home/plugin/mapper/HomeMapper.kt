package com.cinema4you.home.plugin.mapper

import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.model.Movie

class MovieDTOToMovieHomeMapper: Mapper<MovieDTO, Movie> {
    override fun map(input: MovieDTO): Movie {
        return Movie(
            input.id,
            input.overview,
            input.title,
            input.originalTitle,
            input.backdropPath,
            input.posterPath,
            input.releaseDate,
            input.voteAverage
        )
    }
}