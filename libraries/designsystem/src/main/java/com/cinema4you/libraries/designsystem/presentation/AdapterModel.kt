package com.cinema4you.libraries.designsystem.presentation

interface AdapterModel {
    fun layoutId(): Int
    fun isFilterable(filter: String): Boolean
}