package com.cinema4you.features.movie.base.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie")
class MovieDB(
    @PrimaryKey val id: Int,
    val title: String,
    val overview: String,
    val popularity: Float,
    val video: Boolean,
    @SerializedName("vote_average") val voteAverage: Float,
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("original_title") val originalTitle: String,
    @SerializedName("original_language") val originalLanguage: String,
    @SerializedName("vote_count") val voteCount: Int,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("poster_path") val posterPath: String?
)