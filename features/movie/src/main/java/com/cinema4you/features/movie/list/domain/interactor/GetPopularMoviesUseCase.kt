package com.cinema4you.features.movie.list.domain.interactor

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.libraries.core.domain.executor.RXFlowableUseCase
import com.cinema4you.libraries.core.domain.model.Output
import com.cinema4you.libraries.core.plugin.extensions.toResult
import io.reactivex.rxjava3.core.Flowable

@Deprecated("Use CoroutineUseCase")
class GetPopularMoviesUseCase(private val repository: MovieRepository)
    : RXFlowableUseCase<Nothing, List<Movie>>() {
    override fun execute(param: Nothing?): Flowable<Output<List<Movie>>> {
        return repository.getPopularMovies().toResult()
    }
}