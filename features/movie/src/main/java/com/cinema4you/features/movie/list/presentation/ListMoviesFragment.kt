package com.cinema4you.features.movie.list.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.databinding.ContentListMoviesFragmentBinding
import com.cinema4you.features.movie.list.data.MovieAdapterModel
import com.cinema4you.features.movie.list.gateway.ListMoviesViewModel
import com.cinema4you.features.movie.list.presentation.adapter.MovieAdapter
import com.cinema4you.libraries.core.plugin.extensions.hide
import com.cinema4you.libraries.core.plugin.extensions.onObserver
import com.cinema4you.libraries.core.plugin.extensions.show
import com.cinema4you.libraries.designsystem.presentation.GenericAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListMoviesFragment: Fragment(), GenericAdapter.AppAdapterListener<MovieAdapterModel> {

    private var _binding: ContentListMoviesFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModel<ListMoviesViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ContentListMoviesFragmentBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupObservers()
    }

    private fun setupObservers() {
        onObserver(viewModel.moviesList) {
            setMovieListData(it)
        }
        onObserver(viewModel.loading) {
            setUpLoading(it)
        }
    }

    private fun setupViews() {
        binding.moviesListRecyclerView.apply {
            layoutManager =  LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
            setHasFixedSize(true)
        }
    }

    private fun setMovieListData(movies: List<Movie>) {
        val adapter = MovieAdapter(movies, this::onMovieItemClick)
        binding.moviesListRecyclerView.adapter = adapter
    }

    private fun setUpLoading(value: Boolean) {
        if (value) {
            binding.progressCircular.run {
                visibility = View.VISIBLE
            }
            binding.moviesListRecyclerView.hide()
        } else {
            binding.progressCircular.hide()
            binding.moviesListRecyclerView.show()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadPopularMovies()
    }

    private fun onMovieItemClick(movie: Movie) {
        // val action = ListMoviesFragmentDirections.actionMoviesListToMovieDetail(model)
        // view?.findNavController()?.navigate(action)
    }

    override fun onItemClick(model: MovieAdapterModel, position: Int) {
        val action = ListMoviesFragmentDirections.actionMoviesListToMovieDetail(model)
        view?.findNavController()?.navigate(action)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}