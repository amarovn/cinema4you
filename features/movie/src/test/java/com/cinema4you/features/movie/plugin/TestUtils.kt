package com.cinema4you.features.movie.plugin

import com.cinema4you.libraries.core.domain.executor.RXFlowableUseCase
import com.cinema4you.libraries.core.domain.executor.RXMaybeUseCase
import com.cinema4you.libraries.core.domain.executor.RXSingleUseCase
import com.cinema4you.libraries.core.domain.model.Output
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.mockito.Mockito
import org.mockito.ArgumentMatchers.eq

fun <P, R> RXSingleUseCase<P, R>.mockWith(
    param: P?,
    output: Output<R>
    ) {
    Mockito.`when`(this.invoke(eq(param), nonOptionalAny(), nonOptionalAny(), nonOptionalAny()))
        .thenAnswer {
            Single.fromCallable {
                output
            }.subscribeOn(Schedulers.trampoline()).subscribe()
        }
}

fun <P, R> RXMaybeUseCase<P, R>.mockWith(
    param: P?,
    output: Output<R>
) {
    Mockito.`when`(this.invoke(eq(param), nonOptionalAny(), nonOptionalAny(), nonOptionalAny()))
        .thenAnswer {
            Maybe.fromCallable {
                output
            }.subscribeOn(Schedulers.trampoline()).subscribe()
        }
}

fun <P, R> RXFlowableUseCase<P, R>.mockWith(
    param: P?,
    output: Output<R>
) {
    Mockito.`when`(this.invoke(eq(param), nonOptionalAny(), nonOptionalAny(), nonOptionalAny()))
        .thenAnswer {
            Flowable.fromCallable {
                output
            }.subscribeOn(Schedulers.trampoline()).subscribe()
        }
}