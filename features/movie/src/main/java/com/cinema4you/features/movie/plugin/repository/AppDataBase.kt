package com.cinema4you.features.movie.plugin.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cinema4you.features.movie.base.domain.model.MovieDB

private const val DB_NAME = "cinema4you.db"

@Database(entities = [MovieDB::class], version = 1, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract fun movieDao(): MovieDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDataBase? = null

        @JvmStatic
        fun getInstanceDatabase(context: Context): AppDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): AppDataBase {
            return Room.databaseBuilder(context, AppDataBase::class.java, DB_NAME)
                .build()
        }
    }
}