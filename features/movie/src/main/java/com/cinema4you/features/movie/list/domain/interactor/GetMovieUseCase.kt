package com.cinema4you.features.movie.list.domain.interactor

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.libraries.core.domain.executor.RXMaybeUseCase
import com.cinema4you.libraries.core.domain.model.Output
import com.cinema4you.libraries.core.plugin.extensions.toResult
import io.reactivex.rxjava3.core.Maybe
@Deprecated("Use CoroutineUseCase")
internal class GetMovieUseCase(private val repository: MovieRepository
): RXMaybeUseCase<Int, Movie>(){
    override fun execute(param: Int?): Maybe<Output<Movie>> {
        requireNotNull(param)
        return repository.getMovie(param).toResult()
    }
}