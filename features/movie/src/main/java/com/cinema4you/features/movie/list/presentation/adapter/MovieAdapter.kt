package com.cinema4you.features.movie.list.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cinema4.libraries.networking.ImageUrlBuilder
import com.cinema4you.features.movie.R
import com.cinema4you.features.movie.base.domain.model.Movie
import com.google.android.material.imageview.ShapeableImageView

@Deprecated("Use Jetpack Composible")
class MovieAdapter(private val movies: List<Movie>,
                   private val listener: (Movie) -> Unit) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_movie_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MovieAdapter.ViewHolder, position: Int) {
        holder.bind(movies[position], listener)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: Movie, listener: (Movie) -> Unit) {
            val title: TextView = itemView.findViewById(R.id.title)
            title.text = movie.title
            val subtitle: TextView = itemView.findViewById(R.id.subtitle)
            subtitle.text = movie.voteAverage.toString()

            val imageView: ShapeableImageView = itemView.findViewById(R.id.imagePoster)
            Glide.with(itemView)
                .load(movie.posterPath?.let { ImageUrlBuilder.buildPosterUrl(it) })
                .into(imageView)

            itemView.setOnClickListener {
                listener(movie)
            }
        }
    }
}