package com.cinema4you.features.movie.plugin.di

import com.cinema4you.features.movie.list.domain.interactor.GetNowPlayingMoviesUseCase
import com.cinema4you.features.movie.list.domain.interactor.GetPopularMoviesUseCase
import org.koin.dsl.module

val domainModule = module {
    factory { GetPopularMoviesUseCase (get()) }
    factory { GetNowPlayingMoviesUseCase (get()) }
}