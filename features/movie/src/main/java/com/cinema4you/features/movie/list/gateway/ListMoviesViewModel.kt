package com.cinema4you.features.movie.list.gateway

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.list.domain.interactor.GetNowPlayingMoviesUseCase
import com.cinema4you.features.movie.list.domain.interactor.GetPopularMoviesUseCase
import com.cinema4you.libraries.core.domain.model.Output

class ListMoviesViewModel(
    private val getNowPlayingMoviesUseCase: GetNowPlayingMoviesUseCase
) : ViewModel() {

    init {
        loadPopularMovies()
    }

    var listMovieState by mutableStateOf(ListMovieUiState())

    private val _moviesList = MutableLiveData<List<Movie>>()
    val moviesList = _moviesList
    private val _loading = MutableLiveData<Boolean>()
    val loading = _loading

    fun loadPopularMovies() {
        getNowPlayingMoviesUseCase(viewModelScope) {
            success = { listMovieState = ListMovieUiState(it, false) }
        }
    }

    private fun handlePopularMoviesResult(output: Output<List<Movie>>) {
        if (output is Output.Success<List<Movie>>) {
            _moviesList.postValue(output.data!!)
        } else {
            // TODO
        }
    }

    override fun onCleared() {
        // getPopularMoviesUseCase.dispose()
    }
}