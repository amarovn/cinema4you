package com.cinema4you.features.movie.list.domain.interactor


import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.libraries.core.domain.model.Output
import com.cinema4you.libraries.core.plugin.extensions.assert
import io.reactivex.rxjava3.core.Maybe
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

class GetMovieUseCaseTest {

    @Mock
    private lateinit var repository: MovieRepository
    private lateinit var useCase: GetMovieUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        useCase = GetMovieUseCase(repository)
    }

    @Test
    fun `when getMovieUseCase is invoked then should result a movies`() {
        val result = mock<Movie>()
        Mockito.`when`(repository.getMovie(any())).thenReturn(Maybe.just(result))

        useCase.assert(any()) {
            Assert.assertEquals(result, (it as Output.Success).data)
        }
        Mockito.verify(repository, Mockito.times(1)).getMovie(any())
    }

    @Test
    fun `when getMovieUseCase receive a exception then return a fail`() {
        Mockito.`when`(repository.getMovie(any())).thenReturn(Maybe.error(Exception("Mock Error")))

        useCase.assert(any()) {
            Assert.assertTrue(it is Output.Fail)
        }
        Mockito.verify(repository, Mockito.times(1)).getMovie(any())
    }

    @Test
    fun `when getMovieUseCase receive a empty value then return null`() {
        Mockito.`when`(repository.getMovie(any())).thenReturn(Maybe.empty())

        useCase.assert(any()) {
            Assert.assertEquals(null, (it as Output.Success).data)
        }
        Mockito.verify(repository, Mockito.times(1)).getMovie(any())
    }
}