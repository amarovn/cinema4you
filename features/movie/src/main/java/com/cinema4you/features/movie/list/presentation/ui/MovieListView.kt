package com.cinema4you.features.movie.list.presentation.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import com.cinema4.libraries.networking.ImageUrlBuilder
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.list.gateway.ListMoviesViewModel
import com.cinema4you.libraries.designsystem.presentation.ui.bigPadding
import com.cinema4you.libraries.designsystem.presentation.ui.gigaPadding
import com.cinema4you.libraries.designsystem.presentation.ui.smallPadding

@Composable
fun MovieListView(viewModel: ListMoviesViewModel) {
    val listMovieUiState = viewModel.listMovieState
    MovieList(listMovieUiState.data)
}

@Composable
private fun MovieList(movies: List<Movie>) {
    LazyColumn {
        items(movies) { movie ->
            ListItem(movie)
        }
    }
}


@Composable
private fun ListItem(movie: Movie) {
    Card (
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(10.dp))
            .padding(bigPadding),
        elevation = smallPadding
    ) {
        Row {

            AsyncImage (
                model = ImageRequest.Builder(LocalContext.current)
                    .data(movie.posterPath?.let {ImageUrlBuilder.buildPosterUrl(it)})
                    .crossfade(true)
                    .build(),
                contentDescription = "test",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(120.dp, 160.dp)
                    .clip(RoundedCornerShape(10.dp))
            )

            Column {
                Text(
                    text = movie.title,
                    color = Color.Blue,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(0.dp, smallPadding, gigaPadding, smallPadding)
                )
                Text(
                    text = movie.voteAverage.toString(),
                    color = Color.Blue,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(0.dp, smallPadding, gigaPadding, smallPadding)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun Preview() {
    // val viewModel by viewModel<ProfileViewModel>()
    // MovieList()
}