package com.cinema4you.home.plugin.repository

import com.cinema4.libraries.networking.RetrofitConfig
import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.model.Movie

class HomeRepositoryImpl constructor(
    private val retrofitConfig: RetrofitConfig,
    private val mapper: Mapper<MovieDTO,Movie>
): HomeRepository {
    override suspend fun getLastMovie(): Movie? {
        val movie = retrofitConfig.getMovieServiceAPI()
                .getLatestMovie()
                .body()

        return movie?.let { mapper.map(movie) }
    }

    override suspend fun getUpcomingMovies(): List<Movie> {
        return retrofitConfig.getMovieServiceAPI()
            .getUpcomingMovies()
            .body()
            ?.results
            ?.map { mapper.map(it) } ?: emptyList()
    }

    override suspend fun getPopularMovies(): List<Movie> {
        return retrofitConfig.getMovieServiceAPI()
            .getPopularMovies()
            .body()
            ?.results
            ?.map { mapper.map(it) } ?: emptyList()
    }
}