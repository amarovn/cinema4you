package com.cinema4you.home.gateway

import com.cinema4you.home.domain.interactor.GetLastMovieUseCase
import com.cinema4you.home.domain.interactor.GetUpcomingMovieUseCase
import com.cinema4you.home.plugin.MainDispatcherRule
import com.cinema4you.libraries.core.domain.model.Movie
import com.cinema4you.home.plugin.mockWithSuccess
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val getLatestMovieUseCase = mockk<GetLastMovieUseCase>(relaxed = true)
    private val getUpcomingMovieUseCase = mockk<GetUpcomingMovieUseCase>(relaxed = true)
    private lateinit var target : HomeViewModel

    @Before
    fun setUp() {
        target = HomeViewModel(
            getLatestMovieUseCase,
            getUpcomingMovieUseCase
        )
    }

    @Test
    @Ignore
    fun `when getProfileData then should return profile object`() = runTest {
        val movie = mockk<Movie>()

        getLatestMovieUseCase.mockWithSuccess(movie)

        target.loadLastMovie()
        
        // Assert.assertEquals(target.headHomeState, HeadHomeUiState(profile))
    }

    @Test
    fun `when loadUpcomingMovies then should emit HeadHomeUiState`() = runTest {
        //Given
        val movie = getListMovie()

        getUpcomingMovieUseCase.mockWithSuccess(movie)

        //When
        target.loadUpcomingMovies()

        //Verify
        Assert.assertEquals(getHeadHomeUiState(movie, false), target.headHomeState)
    }

    private fun getHeadHomeUiState(movies: List<Movie>, isLoading: Boolean) =
        HeadHomeUiState(
            movies,
            isLoading
        )

    private fun getListMovie() =
        listOf(
            Movie(
                0,
                "",
                "",
                "",
                "",
                "",
                "",
                0f
            )
        )
}
