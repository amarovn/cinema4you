package com.cinema4.libraries.networking.api

import com.cinema4.libraries.networking.BuildConfig
import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4.libraries.networking.model.ResultMovie
import io.reactivex.rxjava3.core.Maybe
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieServiceAPI {
    @Deprecated("use coroutine flow")
    @GET("now_playing")
    fun getNowPlayingMoviesRx(@Query("api_key") apiKey: String = BuildConfig.API_KEY): Maybe<ResultMovie>

    @GET("now_playing")
    suspend fun getNowPlayingMovies(@Query("api_key") apiKey: String = BuildConfig.API_KEY): Response<ResultMovie>

    @GET("latest")
    suspend fun getLatestMovie(@Query("api_key") apiKey: String = BuildConfig.API_KEY): Response<MovieDTO>

    @GET("upcoming")
    suspend fun getUpcomingMovies(@Query("api_key") apiKey: String = BuildConfig.API_KEY): Response<ResultMovie>

    @GET("popular")
    suspend fun getPopularMovies(@Query("api_key") apiKey: String = BuildConfig.API_KEY): Response<ResultMovie>

}