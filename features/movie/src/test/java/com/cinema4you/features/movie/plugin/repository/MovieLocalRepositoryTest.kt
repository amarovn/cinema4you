package com.cinema4you.features.movie.plugin.repository

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.model.MovieDB
import com.cinema4you.features.movie.base.domain.repository.MovieLocalRepository
import com.cinema4you.libraries.core.domain.mapper.ListMapper
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.features.movie.plugin.nonOptionalAny
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock

class MovieLocalRepositoryTest {

    @Mock
    private lateinit var dao: MovieDAO

    @Mock
    private lateinit var mapper: Mapper<MovieDB, Movie>

    @Mock
    private lateinit var listMapper: ListMapper<Movie, MovieDB>

    private lateinit var repository: MovieLocalRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        repository = MovieLocalRepositoryImpl(
            dao,
            mapper,
            listMapper
        )
    }

    @Test
    fun `when saveMovies then should return Complete`() {
        val movieDB: MovieDB = mock()
        Mockito.`when`(listMapper.map(nonOptionalAny())).thenReturn(listOf(movieDB))
        Mockito.`when`(dao.insert(nonOptionalAny())).thenReturn(Completable.complete())

        repository.saveMovies(emptyList()).test().assertComplete()
        Mockito.verify(dao, Mockito.times(1)).insert(nonOptionalAny())
    }

    @Test
    fun `when deleteAllMovies then should return Complete`() {
        Mockito.`when`(dao.deleteAllMovies()).thenReturn(Completable.complete())

        repository.deleteAllMovies().test().assertComplete()
        Mockito.verify(dao, Mockito.times(1)).deleteAllMovies()
    }

    @Test
    fun `when getAllMovies then should return list of movie`() {
        val movieDB: MovieDB = mock()
        val movie: Movie = mock()
        Mockito.`when`(mapper.map(nonOptionalAny())).thenReturn(movie)
        Mockito.`when`(dao.getAllMovies()).thenReturn(Flowable.just(listOf(movieDB)))

        repository.getAllMovies().test().assertValue(listOf(movie))
        Mockito.verify(dao, Mockito.times(1)).getAllMovies()
    }
}