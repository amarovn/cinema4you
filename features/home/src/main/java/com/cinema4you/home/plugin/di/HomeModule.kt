package com.cinema4you.home.plugin.di

import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.home.domain.interactor.GetLastMovieUseCase
import com.cinema4you.home.domain.interactor.GetUpcomingMovieUseCase
import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.home.gateway.HomeViewModel
import com.cinema4you.home.plugin.mapper.MovieDTOToMovieHomeMapper
import com.cinema4you.home.plugin.repository.HomeRepositoryImpl
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.model.Movie
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val MOVIEDTO_MOVIE_HOME_MAPPER = "movieDTOToMovieHomeMapper"

val homeModule = module {
    factory { GetLastMovieUseCase(get()) }
    factory { GetUpcomingMovieUseCase(get()) }

    single { HomeViewModel(get(), get()) }
    single<Mapper<MovieDTO, Movie>>(named(MOVIEDTO_MOVIE_HOME_MAPPER)){ MovieDTOToMovieHomeMapper() }
    single<HomeRepository> { HomeRepositoryImpl(get(), get(named(MOVIEDTO_MOVIE_HOME_MAPPER))) }
}