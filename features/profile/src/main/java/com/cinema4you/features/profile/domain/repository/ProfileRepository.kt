package com.cinema4you.features.profile.domain.repository

import com.cinema4you.features.profile.domain.model.Profile

interface ProfileRepository {
    suspend fun getProfile(): Profile
}