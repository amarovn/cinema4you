package com.cinema4you.features.movie.plugin.repository

import com.cinema4.libraries.networking.RetrofitConfig
import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRemoteRepository
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.mapper.NullableInputListMapper
import io.reactivex.rxjava3.core.Maybe

class MovieRemoteRepositoryImpl(
    private val retrofitConfig: RetrofitConfig,
    private val remoteMapper: NullableInputListMapper<MovieDTO, Movie>,
    private val mapper: Mapper<MovieDTO, Movie>
): MovieRemoteRepository {

    @Deprecated("use coroutine flow")
    override fun fetchMovies(): Maybe<List<Movie>> {
       /* return Maybe.fromCallable { retrofitConfig.getMovieServiceAPI() }
           .flatMap { api -> api.getNowPlayingMovies() }
           .flatMap { result -> Maybe.just(result.results) }
           .map { remoteMapper.map(it) }*/
        return Maybe.just(emptyList())
    }

    override suspend fun fetchMoviesFromRemote(): List<Movie> {
        return retrofitConfig.getMovieServiceAPI()
            .getNowPlayingMovies()
            .body()
            ?.results
            ?.map { mapper.map(it) } ?: emptyList()
    }
}