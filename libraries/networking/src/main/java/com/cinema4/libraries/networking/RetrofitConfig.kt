package com.cinema4.libraries.networking

import com.cinema4.libraries.networking.api.MovieServiceAPI

interface RetrofitConfig {
    suspend fun getMovieServiceAPI(): MovieServiceAPI
}