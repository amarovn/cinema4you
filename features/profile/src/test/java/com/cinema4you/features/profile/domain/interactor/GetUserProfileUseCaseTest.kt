package com.cinema4you.features.profile.domain.interactor

import com.cinema4you.features.profile.domain.model.Profile
import com.cinema4you.features.profile.domain.repository.ProfileRepository
import com.cinema4you.features.profile.plugin.MainDispatcherRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GetUserProfileUseCaseTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val repository = mockk<ProfileRepository>()
    lateinit var target: GetUserProfileUseCase

    @Before
    fun setUp() {
        target = GetUserProfileUseCase(repository)
    }

    @Test
    fun `when getProfile then should return correct data`() = runTest {
        val result = Profile("", "", "")
        coEvery { repository.getProfile() } returns result

        target(this) {
            success = {
                Assert.assertEquals(result, it)
            }
        }
    }

    @Test
    fun `when getProfile throw a exception then usecase should fail`() = runTest {
        val result = Exception()
        coEvery { repository.getProfile() } throws result

        target(this) {
            fail = {
                Assert.assertEquals(result, it)
            }
        }
    }
}