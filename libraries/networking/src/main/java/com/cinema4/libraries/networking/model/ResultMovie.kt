package com.cinema4.libraries.networking.model

data class ResultMovie(val results: MutableList<MovieDTO>)