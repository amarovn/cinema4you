package com.cinema4you.home.domain.interactor

import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.home.plugin.MainDispatcherRule
import com.cinema4you.libraries.core.domain.model.Movie
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GetUpcomingMovieUseCaseTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val repository = mockk<HomeRepository>()

    lateinit var target: GetUpcomingMovieUseCase

    @Before
    fun setUp() {
        target = GetUpcomingMovieUseCase(repository)
    }

    @Test
    fun `when getUpcomingMovies then should return correct data with success`() = runTest {
        val result = mockk<List<Movie>>()
        coEvery { repository.getUpcomingMovies() } returns result

        target(this) {
            success = {
                Assert.assertEquals(result, it)
            }
        }
    }

    @Test
    fun `when getUpcomingMovies throw a exception then usecase should fail`() = runTest {
        val result = Exception()
        coEvery { repository.getUpcomingMovies() } throws result

        target(this) {
            fail = {
                Assert.assertEquals(result, it)
            }
        }
    }
}