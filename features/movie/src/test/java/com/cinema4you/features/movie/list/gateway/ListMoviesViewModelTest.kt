package com.cinema4you.features.movie.list.gateway

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.list.domain.interactor.GetPopularMoviesUseCase
import com.cinema4you.libraries.core.domain.model.Output

import com.cinema4you.features.movie.plugin.mockWith
import com.cinema4you.features.movie.plugin.nonOptionalAny
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ListMoviesViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var getPopularMoviesUseCase: GetPopularMoviesUseCase

    private lateinit var viewModel: ListMoviesViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        viewModel = ListMoviesViewModel(getPopularMoviesUseCase)
    }

    @Test
    fun `when loadPopularMoview then should call getPopularMoviesUseCase`() {
        val result = listOf<Movie>()
        val data = Output.Success(result)

        getPopularMoviesUseCase.mockWith(null, data)

        viewModel.loadPopularMovies()

        Mockito.verify(getPopularMoviesUseCase, Mockito.times(1)).invoke(
            ArgumentMatchers.eq(null),
            nonOptionalAny(),
            nonOptionalAny(),
            nonOptionalAny()
        )
    }
}