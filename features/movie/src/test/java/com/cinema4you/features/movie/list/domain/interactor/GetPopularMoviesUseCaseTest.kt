package com.cinema4you.features.movie.list.domain.interactor

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.libraries.core.domain.model.Output
import com.cinema4you.libraries.core.plugin.extensions.assert
import io.reactivex.rxjava3.core.Flowable
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class GetPopularMoviesUseCaseTest {

    @Mock
    private lateinit var repository: MovieRepository
    private lateinit var useCase: GetPopularMoviesUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        useCase = GetPopularMoviesUseCase(repository)
    }

    @Test
    fun `when getPopularMoviesUseCase is invoked then should result list of movies`() {
        val result = listOf<Movie>()
        Mockito.`when`(repository.getPopularMovies()).thenReturn(Flowable.just(result))

        useCase.assert(null) {
            assertEquals(result, (it as Output.Success).data)
        }
        verify(repository, times(1)).getPopularMovies()
    }

    @Test
    fun `when getPopularMoviesUseCase receive a exception then return a fail`() {
        Mockito.`when`(repository.getPopularMovies()).thenReturn(Flowable.error(Exception("Mock Error")))

        useCase.assert(null) {
            assertTrue(it is Output.Fail)
        }
        verify(repository, times(1)).getPopularMovies()
    }
}