package com.cinema4you.features.movie.base.domain.model

import androidx.annotation.Keep

@Keep
data class Movie (
    var id: Int,
    var overview: String,
    var title: String,
    var originalTitle: String,
    var backdropPath: String?,
    var posterPath: String?,
    var releaseDate: String,
    var voteAverage: Float
)