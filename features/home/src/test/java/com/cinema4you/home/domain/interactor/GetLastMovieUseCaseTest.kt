package com.cinema4you.home.domain.interactor

import com.cinema4you.home.domain.repository.HomeRepository
import com.cinema4you.home.plugin.MainDispatcherRule
import com.cinema4you.libraries.core.domain.model.Movie
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class GetLastMovieUseCaseTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val repository = mockk<HomeRepository>()

    lateinit var target: GetLastMovieUseCase

    @Before
    fun setUp() {
        target = GetLastMovieUseCase(repository)
    }

    @Test
    fun `when getLastMovie then should return correct data with success`() = runTest {
        val result = mockk<Movie>()
        coEvery { repository.getLastMovie() } returns result

        target(this) {
            success = {
                Assert.assertEquals(result, it)
            }
        }
    }

    @Test
    fun `when getLastMovie throw a exception then usecase should fail`() = runTest {
        val result = Exception()
        coEvery { repository.getLastMovie() } throws result

        target(this) {
            fail = {
                Assert.assertEquals(result, it)
            }
        }
    }
}