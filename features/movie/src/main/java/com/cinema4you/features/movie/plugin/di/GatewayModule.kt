package com.cinema4you.features.movie.plugin.di

import com.cinema4you.features.movie.list.gateway.ListMoviesViewModel
import org.koin.dsl.module

val gatewayModule = module {
    single { ListMoviesViewModel(get()) }
}