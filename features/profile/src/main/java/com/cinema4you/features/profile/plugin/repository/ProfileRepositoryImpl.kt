package com.cinema4you.features.profile.plugin.repository

import com.cinema4you.features.profile.domain.model.Profile
import com.cinema4you.features.profile.domain.repository.ProfileRepository

class ProfileRepositoryImpl: ProfileRepository {
    override suspend fun getProfile(): Profile {
        return Profile(
            "Amaro Neto",
            "amarovn@gmail.com",
            "Brazil"
        )
    }
}