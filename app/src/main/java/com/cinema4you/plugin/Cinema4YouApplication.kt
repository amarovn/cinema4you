package com.cinema4you.plugin

import android.app.Application
import com.cinema4you.features.movie.plugin.di.daoModule
import com.cinema4you.features.movie.plugin.di.domainModule
import com.cinema4you.features.movie.plugin.di.gatewayModule
import com.cinema4you.features.movie.plugin.di.repositoryModule
import com.cinema4you.features.profile.plugin.di.profileDomainModule
import com.cinema4you.features.profile.plugin.di.profileGatewayModule
import com.cinema4you.features.profile.plugin.di.profileRepositoryModule
import com.cinema4you.home.plugin.di.homeModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class Cinema4YouApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@Cinema4YouApplication)
            androidFileProperties()
            modules(listOf(
                daoModule,
                repositoryModule,
                domainModule,
                gatewayModule,
                profileDomainModule,
                profileRepositoryModule,
                profileGatewayModule,
                homeModule
            ))
        }
    }
}