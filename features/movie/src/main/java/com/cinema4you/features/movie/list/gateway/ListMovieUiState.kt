package com.cinema4you.features.movie.list.gateway

import com.cinema4you.features.movie.base.domain.model.Movie

data class ListMovieUiState(
    val data: List<Movie> = emptyList(),
    val loading: Boolean = false
    // TODO error state
)
