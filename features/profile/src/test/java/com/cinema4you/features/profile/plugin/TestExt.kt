package com.cinema4you.features.profile.plugin

import com.cinema4you.libraries.core.domain.executor.CoroutineUseCase
import io.mockk.coEvery
import java.lang.Exception

suspend fun <P, R: Any> CoroutineUseCase<P,R>.mockWithSuccess(output: R) {
    val useCase = this
    val result = CoroutineUseCase.UseCaseResult<R>()
    coEvery { useCase.invoke(any(), any(), any(), any()) } coAnswers {
        lastArg<CoroutineUseCase.UseCaseResult<R>.() -> Unit>().invoke(result)
        result.onSuccess(output)
    }
}

suspend fun <P, R: Any> CoroutineUseCase<P,R>.mockWithFail(exception: Exception) {
    val useCase = this
    val result = CoroutineUseCase.UseCaseResult<R>()
    coEvery { useCase.invoke(any(), any(), any(), any()) } coAnswers {
        lastArg<CoroutineUseCase.UseCaseResult<R>.() -> Unit>().invoke(result)
        result.onFail(exception)
    }
}