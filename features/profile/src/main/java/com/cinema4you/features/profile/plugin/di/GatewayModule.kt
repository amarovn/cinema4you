package com.cinema4you.features.profile.plugin.di

import com.cinema4you.features.profile.gateway.ProfileViewModel
import org.koin.dsl.module

val profileGatewayModule = module {
    single { ProfileViewModel(get()) }
}