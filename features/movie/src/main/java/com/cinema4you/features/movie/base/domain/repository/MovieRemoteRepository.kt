package com.cinema4you.features.movie.base.domain.repository

import com.cinema4you.features.movie.base.domain.model.Movie
import io.reactivex.rxjava3.core.Maybe

interface MovieRemoteRepository {
    fun fetchMovies(): Maybe<List<Movie>>
    suspend fun fetchMoviesFromRemote(): List<Movie>
}