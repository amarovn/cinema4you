package com.cinema4you.home.domain.repository

import com.cinema4you.libraries.core.domain.model.Movie

interface HomeRepository {
    suspend fun getLastMovie(): Movie?
    suspend fun getUpcomingMovies(): List<Movie>
    suspend fun getPopularMovies(): List<Movie>
}