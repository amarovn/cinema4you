package com.cinema4you.features.profile.gateway

import com.cinema4you.features.profile.domain.model.Profile

data class UiState (
        val data: Profile = Profile(),
        val loading: Boolean = false
        // TODO error state
    )
