package com.cinema4you.features.movie.plugin.mapper

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.model.MovieDB
import com.cinema4you.libraries.core.domain.mapper.ListMapper
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.mapper.NullableInputListMapper

class MovieToMovieDBMapper: Mapper<MovieDB, Movie> {
    override fun map(input: MovieDB): Movie {
        return Movie(
            input.id,
            input.overview,
            input.title,
            input.originalTitle,
            input.backdropPath,
            input.posterPath,
            input.releaseDate,
            input.voteAverage
        )
    }
}


class MovieListToMovieDBListMapper: ListMapper<Movie, MovieDB> {
    override fun map(input: List<Movie>): List<MovieDB> {
        return input.map {
            MovieDB(
                it.id,
                it.title,
                it.overview,
                0f,
                false,
                it.voteAverage,
                it.backdropPath,
                it.originalTitle,
                "",
                0,
                it.releaseDate,
                it.posterPath
            )
        }
    }
}

class MovieDBListMapper: ListMapper<MovieDB, Movie> {
    override fun map(input: List<MovieDB>): List<Movie> {
        return input.map {
            Movie(
                it.id,
                it.overview,
                it.title,
                it.originalTitle,
                it.backdropPath,
                it.posterPath,
                it.releaseDate,
                it.voteAverage
            )
        }
    }
}

class MovieDBNullableInputListMapper: NullableInputListMapper<MovieDB, Movie> {
    override fun map(input: List<MovieDB>?): List<Movie> {
        return  input?.map { Movie(
            it.id,
            it.overview,
            it.title,
            it.originalTitle,
            it.backdropPath,
            it.posterPath,
            it.releaseDate,
            it.voteAverage
        ) }.orEmpty()
    }
}