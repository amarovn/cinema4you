package com.cinema4you.features.movie.base.domain.repository

import com.cinema4you.features.movie.base.domain.model.Movie
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

interface MovieLocalRepository {
    fun saveMovies(movies: List<Movie>): Completable
    fun deleteAllMovies(): Completable
    fun getAllMovies(): Flowable<List<Movie>>
    suspend fun save(movies: List<Movie>)
    suspend fun deleteAll()
    suspend fun getAll(): List<Movie>
}