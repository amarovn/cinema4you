package com.cinema4you.features.profile.domain.interactor

import com.cinema4you.features.profile.domain.model.Profile
import com.cinema4you.features.profile.domain.repository.ProfileRepository
import com.cinema4you.libraries.core.domain.executor.CoroutineUseCase
import com.cinema4you.libraries.core.domain.model.Output

class GetUserProfileUseCase constructor(
    private val repository: ProfileRepository
    ) : CoroutineUseCase<CoroutineUseCase.None, Profile>() {

    override suspend fun execute(param: None?): Profile {
        return repository.getProfile()
    }
}