package com.cinema4you.features.movie.list.domain.interactor

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.libraries.core.domain.executor.CoroutineUseCase

class GetNowPlayingMoviesUseCase constructor(
    private val repository: MovieRepository
    ) : CoroutineUseCase<CoroutineUseCase.None,List<Movie>>() {
    override suspend fun execute(param: None?): List<Movie> {
        return repository.getPopularMoviesList()
    }
}