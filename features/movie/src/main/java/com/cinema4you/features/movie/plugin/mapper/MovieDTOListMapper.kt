package com.cinema4you.features.movie.plugin.mapper

import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.libraries.core.domain.mapper.ListMapper
import com.cinema4you.libraries.core.domain.mapper.Mapper
import com.cinema4you.libraries.core.domain.mapper.NullableInputListMapper

class MovieRemoteListMapper: ListMapper<MovieDTO, Movie> {
    override fun map(input: List<MovieDTO>): List<Movie> {
        return input.map {
            Movie(
                it.id,
                it.overview,
                it.title,
                it.originalTitle,
                it.backdropPath,
                it.posterPath,
                it.releaseDate,
                it.voteAverage
            )
        }
    }
}

class MovieRemoteNullableInputListMapper: NullableInputListMapper<MovieDTO, Movie> {
    override fun map(input: List<MovieDTO>?): List<Movie> {
        return  input?.map { Movie(
            it.id,
            it.overview,
            it.title,
            it.originalTitle,
            it.backdropPath,
            it.posterPath,
            it.releaseDate,
            it.voteAverage
        ) }.orEmpty()
    }
}

class MovieRemoteMapper: Mapper<MovieDTO, Movie> {
    override fun map(input: MovieDTO): Movie {
        return  Movie(
            input.id,
            input.overview,
            input.title,
            input.originalTitle,
            input.backdropPath,
            input.posterPath,
            input.releaseDate,
            input.voteAverage
        )
    }
}