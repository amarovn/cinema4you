package com.cinema4you.features.movie.plugin.repository

import com.cinema4.libraries.networking.NetworkUtils
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieLocalRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRemoteRepository
import com.cinema4you.features.movie.base.domain.repository.MovieRepository
import com.cinema4you.features.movie.plugin.RepositoryTestRule
import com.cinema4you.features.movie.plugin.nonOptionalAny
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock

class MovieRepositoryImplTest {

    @Mock
    private lateinit var remoteRepository: MovieRemoteRepository

    @Mock
    private lateinit var localRepository: MovieLocalRepository

    @Mock
    private lateinit var networkUtils: NetworkUtils

    @get:Rule
    val repositoryTestRule = RepositoryTestRule()

    private lateinit var repository: MovieRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        repository = MovieRepositoryImpl(
            repositoryTestRule.context,
            networkUtils,
            remoteRepository,
            localRepository
        )
    }

    @Test
    fun `when getPopularMovies with internet connection then should return movies from remote`() {
        val movie: Movie = mock()
        val result = listOf(movie)

        Mockito.`when`(networkUtils.hasInternetConnection(repositoryTestRule.context)).thenReturn(true)
        Mockito.`when`(remoteRepository.fetchMovies()).thenReturn(Maybe.just(result))
        Mockito.`when`(localRepository.deleteAllMovies()).thenReturn(Completable.complete())
        Mockito.`when`(localRepository.saveMovies(nonOptionalAny())).thenReturn(Completable.complete())

        repository.getPopularMovies().test().assertValue(result)

        Mockito.verify(localRepository, Mockito.times(1)).deleteAllMovies()
        Mockito.verify(localRepository, Mockito.times(1)).saveMovies(nonOptionalAny())
    }

    @Test
    fun `when getPopularMovies without internet connection then should return movies from local`() {
        val movie: Movie = mock()
        val result = listOf(movie)

        Mockito.`when`(networkUtils.hasInternetConnection(repositoryTestRule.context)).thenReturn(false)
        Mockito.`when`(remoteRepository.fetchMovies()).thenReturn(Maybe.just(result))
        Mockito.`when`(localRepository.getAllMovies()).thenReturn(Flowable.just(result))
        repository.getPopularMovies().test().assertValue(result)
    }
}