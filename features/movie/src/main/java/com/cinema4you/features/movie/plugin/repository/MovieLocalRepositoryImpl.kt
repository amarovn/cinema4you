package com.cinema4you.features.movie.plugin.repository

import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.model.MovieDB
import com.cinema4you.features.movie.base.domain.repository.MovieLocalRepository
import com.cinema4you.libraries.core.domain.mapper.ListMapper
import com.cinema4you.libraries.core.domain.mapper.Mapper
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

class MovieLocalRepositoryImpl(
    private val movieDAO: MovieDAO,
    private val movieDBMapper: Mapper<MovieDB, Movie>,
    private val movieMapper: ListMapper<Movie, MovieDB>
    ) : MovieLocalRepository {

    override fun saveMovies(movies: List<Movie>): Completable {
        return movieDAO.insert(movieMapper.map(movies))
    }

    override fun deleteAllMovies(): Completable {
        return movieDAO.deleteAllMovies()
    }

    override fun getAllMovies(): Flowable<List<Movie>> {
       return movieDAO.getAllMovies().map { it.map { movie ->
           movieDBMapper.map(movie)
       } }
    }

    override suspend fun save(movies: List<Movie>) {
        movieDAO.save(movieMapper.map(movies))
    }

    override suspend fun deleteAll() {
        return movieDAO.deleteAll()
    }

    override suspend fun getAll(): List<Movie> {
        return movieDAO.getAll().map {
            movieDBMapper.map(it)
        }
    }
}