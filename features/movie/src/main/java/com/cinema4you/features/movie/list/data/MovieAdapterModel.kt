package com.cinema4you.features.movie.list.data

import androidx.annotation.Keep
import com.cinema4you.features.movie.R
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.libraries.designsystem.presentation.AdapterModel

@Keep
class MovieAdapterModel(
    val title: String = "",
    val description: String = ""
) : AdapterModel {

    override fun layoutId(): Int = R.layout.movie_adapter_model

    override fun isFilterable(filter: String): Boolean {
        TODO("Not yet implemented")
    }

    companion object {
        private fun convert(movie: Movie) = MovieAdapterModel(movie.title, movie.releaseDate)

        fun convertList(movies: List<Movie>) = movies.map { convert(it) }
    }
}