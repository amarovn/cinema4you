package com.cinema4you.features.profile.plugin.di

import com.cinema4you.features.profile.domain.interactor.GetUserProfileUseCase
import org.koin.dsl.module

val profileDomainModule = module {
    factory { GetUserProfileUseCase (get()) }
}