package com.cinema4you.features.movie.base.domain.repository

import com.cinema4you.features.movie.base.domain.model.Movie
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe

interface MovieRepository {
    fun getPopularMovies() : Flowable<List<Movie>>
    fun getMovie(id: Int) : Maybe<Movie>
    suspend fun getPopularMoviesList(): List<Movie>
}