package com.cinema4you.home.presentation

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import com.cinema4you.features.movie.list.gateway.ListMoviesViewModel
import com.cinema4you.features.profile.gateway.ProfileViewModel
import com.cinema4you.home.gateway.HomeViewModel
import com.cinema4you.libraries.designsystem.presentation.ui.Cinema4YouTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val moviesViewModel by viewModel<ListMoviesViewModel>()
    private val viewModel by viewModel<ProfileViewModel>()
    private val homeViewModel by viewModel<HomeViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.home_activity)
        setContent {
            Cinema4YouTheme(true) {
                // ProfileView(viewModel)
                // MovieListView(moviesViewModel)
                HomeView(viewModel = homeViewModel)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        homeViewModel.loadUpcomingMovies()
    }
}