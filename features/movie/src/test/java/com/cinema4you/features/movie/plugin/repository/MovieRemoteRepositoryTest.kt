package com.cinema4you.features.movie.plugin.repository

import com.cinema4.libraries.networking.RetrofitConfig
import com.cinema4.libraries.networking.api.MovieServiceAPI
import com.cinema4.libraries.networking.model.MovieDTO
import com.cinema4.libraries.networking.model.ResultMovie
import com.cinema4you.features.movie.base.domain.model.Movie
import com.cinema4you.features.movie.base.domain.repository.MovieRemoteRepository
import com.cinema4you.libraries.core.domain.mapper.NullableInputListMapper
import com.cinema4you.features.movie.plugin.nonOptionalAny
import io.reactivex.rxjava3.core.Maybe
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.mock

class MovieRemoteRepositoryTest {

    @Mock
    private lateinit var retrofitConfig: RetrofitConfig

    @Mock
    private lateinit var movieAPI: MovieServiceAPI

    @Mock
    private lateinit var mapper: NullableInputListMapper<MovieDTO, Movie>

    private lateinit var repository: MovieRemoteRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        repository = MovieRemoteRepositoryImpl(
            retrofitConfig,
            mapper
        )
    }

    @Test
    fun `when fetchMovies then should return movies`() {
        val result: ResultMovie = mock()
        val movie: Movie = mock()

        Mockito.`when`(retrofitConfig.getMovieServiceAPI()).thenReturn(movieAPI)
        Mockito.`when`(movieAPI.getNowPlayingMoviesRx(nonOptionalAny())).thenReturn(Maybe.just(result))
        Mockito.`when`(mapper.map(nonOptionalAny())).thenReturn(listOf(movie))

        repository.fetchMovies().test().assertValue(listOf(movie))
    }
}