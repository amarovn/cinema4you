package com.cinema4you.features.profile.domain.model

import androidx.annotation.Keep

@Keep
data class Profile(
    val name: String = "",
    val email: String = "",
    val country: String = ""
)